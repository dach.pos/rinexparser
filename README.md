# RinexParser

Python scripts to analyse Rinex data. Supports Rinex 2 and Rinex 3. It generates a RinexParser class that holds a datadict with all Rinex Information (RinexHeader, RinexObs).

## Further thoughts

At first it used pandas to hold the rinex observations. Since the parsing of huge files took too long i switched to native dictionary to hold the data.

# Install

## via Git

```

git clone https://gitlab.com/dach.pos/rinexparser.git
cd rinexparser
make cleanAll
make prepareVenv
source env/bin/activate
make setupVenv
make test

```

If the test was successfull:

``` 

python setup.py install

```

## via PyPi

```
python -m pip install RinexParser
```



Within your program you can then import the package.

# Example

## Simple Usage

```
from rinex_parser.obs_parser import RinexParser

# define version and file path
rnx_version = 3
rnx_file = "PATH/TO/YOUR/RINEX_FILE"
# initialise parser
obs_parser = RinexParser(rinex_version=rnx_version, rinex_file=rnx_file)
obs_parser.run()
# get output
print(obs_parser.datadict)
# datadict: {
#    "epochs": [
#        {
#            "id": "YYYY-mm-ddTHH:MM:SSZ", 
#            "satellites": [
#                {
#                    "id": "<Satellite Number>",
#                    "observations": {
#                        "<Observation Descriptor>": {
#                            "value": ..,
#                            "lli": ..,
#                            "ss": ..
#                        }
#                    }
#                },
#                {..}
#            ]
#        }
# ]}

```

## Further Examples

Please check the file *tests/test_obs_reader.py* for examples

Have Fun!
